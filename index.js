const express = require('express');
const path = require('path');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

/**
 * Serve single page application on all routes
 */
app.use('/', express.static(path.join(__dirname, '/build')));
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '/build/index.html'));
});

const messages = [];
const clients = {};
const maxMessages = 1000;

io.on('connection', socket => {
    console.log(`SocketID ${socket.id} is connected`);
    clients[socket.id] = socket;

    socket.on('messages.create', (msg, resolve)=> {
        msg.createdAt = Date.now();
        if (!msg.user) {
            msg.user = { name: 'unknown user'}
        }
        msg.user.id = socket.id;

        messages.push(msg);
        if (messages.length > maxMessages) {
            messages.splice(maxMessages, messages.length - 1);
        }
        io.emit('message', msg);
        resolve();
    });

    socket.on('messages.read', resolve => {
        console.log('messages.read', messages);
        resolve(messages);
    });

    socket.on('disconnect', () => {
        console.log(`SocketID ${socket.id} is disconnected`);
        delete clients[socket.id];
    })
});

/**
 * Fire it up
 */
http.listen(process.env.PORT || 3000, () => {
    console.info(`Server listening on :${http.address().port}`);
});