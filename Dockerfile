FROM node:latest
MAINTAINER Tim Kolberger <tim@kolberger.eu>

RUN npm install -g \
    nodemon \
    react-scripts

ENV APPENV=prod PORT=80
ENV TZ=Europe/Berlin
RUN echo $TZ | tee /etc/timezone
RUN dpkg-reconfigure --frontend noninteractive tzdata

EXPOSE $PORT

ENTRYPOINT node index.js