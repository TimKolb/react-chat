import io from 'socket.io-client';
import config from '../config';
export const socket = io.connect(config.socketAddress);

export const RECEIVE_MESSAGES = 'RECEIVE_MESSAGES';
export const REQUEST_MESSAGES = 'REQUEST_MESSAGES';
export const RECEIVE_SINGLE_MESSAGE = 'RECEIVE_SINGLE_MESSAGE';
export const TYPE_MESSAGE = 'TYPE_MESSAGE';
export const MESSAGE_SENT = 'SENT_MESSAGE';
export const LOAD_USERNAME = 'LOAD_USERNAME';
export const OPEN_MODAL = 'OPEN_MODAL';
export const CLOSE_MODAL = 'CLOSE_MODAL';

export const SET_USERNAME = 'SET_USERNAME';

function requestMessages() {
    return {
        type: REQUEST_MESSAGES
    }
}

function receiveMessages(messages) {
    return {
        type: RECEIVE_MESSAGES,
        receivedAt: Date.now(),
        messages
    }
}

function receiveMessage(message) {
    return {
        type: RECEIVE_SINGLE_MESSAGE,
        message
    };
}

export function fetchMessages() {
    return function (dispatch) {
        dispatch(requestMessages());
        socket.emit('messages.read', messages => {
            dispatch(receiveMessages(messages));
        });
    };
}

export function sendMessage(message) {
    return function (dispatch) {
        socket.emit('messages.create', message, () => dispatch({type: MESSAGE_SENT}));
    };
}

export function openModal() {
    return {
        type: OPEN_MODAL
    }
}

export function closeModal() {
    return {
        type: CLOSE_MODAL
    }
}

export function typeMessage(value) {
    return {
        type: TYPE_MESSAGE,
        value
    }
}

export function listenForNewMessages() {
    return function (dispatch) {
        socket.on('message', message => dispatch(receiveMessage(message)));
    }
}

export function setUserName(username) {
    return {
        type: SET_USERNAME,
        username
    }
}

export function loadUser() {
    return {
        type: LOAD_USERNAME
    }
}