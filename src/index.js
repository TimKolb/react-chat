import ReactDOM from 'react-dom';
import React from 'react';
import {Router, Route, IndexRoute, browserHistory} from 'react-router';

import Layout from './components/containers/Layout';
import Chat from './components/containers/Chat';

import injectTapEventPlugin from 'react-tap-event-plugin';
import './styles/index.css';

import {Provider} from 'react-redux';
import store from './store';

import './styles.css'

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

ReactDOM.render(
    <Provider store={store}>
        <Router history={browserHistory}>
                <Layout>
                    <Route path='/' component={Layout}>
                        <IndexRoute component={Chat}/>
                    </Route>
                </Layout>
        </Router>
    </Provider>,
    document.getElementById('root')
);