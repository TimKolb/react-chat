import React from 'react';
import {
    Dialog,
    FlatButton,
    TextField
} from 'material-ui';

import {connect} from 'react-redux';
import {setUserName, openModal, closeModal} from '../actions';

export class Register extends React.Component {
    state = {
        username: ''
    };

    handleOpen = () => {
        const {dispatch} = this.props;
        dispatch(openModal());
    };

    handleClose = () => {
        const {dispatch} = this.props;
        dispatch(closeModal());
    };

    handleChangeName = (event, username) => {
        this.setState({username});
    };

    handleSave = () => {
        const {dispatch} = this.props;
        dispatch(setUserName(this.state.username));
        this.handleClose();
    };

    handleKeyPress = (event) => {
        if (event.key === 'Enter') {
            this.handleSave();
        }
    };

    render() {
        const actions = [
            <FlatButton
                label="Cancel"
                onTouchTap={this.handleClose}
            />,
            <FlatButton
                label="Save"
                primary={true}
                keyboardFocused={true}
                onTouchTap={this.handleSave}
            />
        ];

        return (
            <div style={this.props.style}>
                <FlatButton style={this.props.buttonStyle} label="Register" onTouchTap={this.handleOpen}/>
                <Dialog
                    title="Register"
                    actions={actions}
                    modal={false}
                    open={!!this.props.user && this.props.user.registerModal}
                    onRequestClose={this.handleClose}
                >
                    <TextField
                        floatingLabelText="Username"
                        onChange={this.handleChangeName}
                        value={this.state.username}
                        onKeyPress={this.handleKeyPress}
                    />
                </Dialog>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => ({dispatch});

const mapStateToProps = state => ({
    user: state.user
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Register);