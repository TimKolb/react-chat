import React, {PropTypes, Component} from 'react';
import {
    AppBar,
    IconButton,
    List,
    ListItem,
    Paper,
    TextField,
    Badge
} from 'material-ui';
import * as colors from 'material-ui/styles/colors';
import Send from 'material-ui/svg-icons/content/send';
import Message from '../Message';
import NotificationsIcon from 'material-ui/svg-icons/social/notifications';

import {connect} from 'react-redux';
import {fetchMessages, sendMessage, typeMessage} from '../../actions';

import Register from '../Register';

const headerHeight = '70px';
const inputHeight = '50px';
const sidebarStyles = {
    backgroundColor: colors.indigo800,
    width: '300px',
    height: '100%'
};

const styles = {
    container: {},
    chatInputContainer: {
        position: 'fixed',
        bottom: 0,
        left: 0,
        right: 0,
        height: inputHeight,
        lineHeight: '50px',
        padding: 0,
        display: 'block'
    },
    chatInput: {
        height: inputHeight,
        padding: '0 0 0 15px',
        display: 'block',
        width: 'calc(100% - 68px)',
        float: 'left'
    },
    messageContainer: {
        position: 'relative',
        backgroundColor: colors.darkWhite,
        paddingTop: headerHeight,
        paddingBottom: inputHeight
    },
    sidebar: sidebarStyles,
    header: {
        position: 'fixed',
        height: headerHeight
    },
    headerTitle: {
        color: '#FFF'
    },
    searchContainer: {
        backgroundColor: colors.indigo800,
        textAlign: 'center',
        paddingTop: '10px'
    },
    searchInput: {
        color: colors.white
    },
    listItem: {
        color: colors.darkWhite
    },
    secondaryText: {
        color: colors.grey400
    },
    messageList: {
        maxHeight: 'calc(100vh - 135px)',
        overflowY: 'scroll'
    }
};

export class Chat extends Component {

    static propTypes = {
        messages: PropTypes.array.isRequired,
        input: PropTypes.string.isRequired,
        dispatch: PropTypes.func.isRequired
    };

    componentWillMount() {
        const {dispatch} = this.props;
        dispatch(fetchMessages());
    }

    componentDidMount() {
        this.refs.nameInput.focus();
    }

    componentDidUpdate() {
    }

    handleSendMessage = () => {
        const {dispatch, input} = this.props;
        if (!input) {
            return false;
        }
        const message = {
            content: input,
            user: this.props.user
        };
        dispatch(sendMessage(message));
        this.refs.nameInput.focus();
    };

    handleKeyPress = (event) => {
        if (event.key === 'Enter') {
            return this.handleSendMessage();
        }
    };

    handleInputChange = (event, value) => {
        const {dispatch} = this.props;
        dispatch(typeMessage(value));
    };

    render() {
        let children = this.props.messages.map((item, key) => (
            <Message key={key} message={item}/>)
        );

        if (children.length < 1) {
            children = (<ListItem style={{textAlign: 'center'}}>No messages</ListItem>);
        }
        return (
            <section>
                <AppBar
                    style={styles.header}
                    titleStyle={styles.headerTitle}
                    title={<span style={styles.title}>Chat</span>}
                    iconElementRight={<Register buttonStyle={{color: '#FFF'}}/>}
                    iconElementLeft={<Badge badgeContent={this.props.messages.length}><NotificationsIcon style={{color: '#FFF'}}/></Badge>}
                />
                <Paper style={styles.messageContainer}>
                    <List style={styles.messageList}>
                        {children}
                    </List>
                </Paper>
                <Paper style={styles.chatInputContainer}>
                    <TextField
                        style={styles.chatInput}
                        underlineShow={false}
                        hintText="Message"
                        value={this.props.input}
                        onChange={this.handleInputChange}
                        onKeyPress={this.handleKeyPress}
                        ref='nameInput'
                    />
                    <IconButton disabled={!this.props.input}>
                        <Send color={colors.blueA200}
                              onClick={this.handleSendMessage}/>
                    </IconButton>
                </Paper>
            </section>
        );

    }
}

const mapStateToProps = state => ({
    messages: state.messages.all,
    input: state.messages.input,
    user: state.user
});

const mapDispatchToProps = dispatch => ({dispatch});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Chat);
