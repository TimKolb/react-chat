import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {
    grey100,
    grey500,
    white,
    grey300,
    darkBlack,
    fullBlack
} from 'material-ui/styles/colors';
import DevTools from './DevTools';

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: '#273648',
        //primary2Color: '#47cadb',
        //primary3Color: '#7d8691',
        accent1Color: '#47cadb',
        accent2Color: grey100,
        accent3Color: grey500,
        textColor: darkBlack,
        alternateTextColor: white,
        canvasColor: white,
        borderColor: grey300,
        //disabledColor: fade(darkBlack, 0.3),
        //pickerHeaderColor: green500,
        //clockCircleColor: fade(darkBlack, 0.07),
        shadowColor: fullBlack,
    }
});

export default class Layout extends React.Component {
    static childContextTypes = {
        muiTheme: React.PropTypes.object
    };

    constructor(props) {
        super(props);
        this.state = {navOpen: true};
    }

    menuToggled = (state) => {
        this.setState({navOpen: state});
    };

    render() {
        return (
            <MuiThemeProvider muiTheme={muiTheme}>
                <div>
                    <DevTools />
                    {this.props.children}
                </div>
            </ MuiThemeProvider>
        );
    }
}