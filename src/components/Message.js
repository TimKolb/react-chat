import React, {PropTypes, Component} from 'react';
import {
    ListItem,
} from 'material-ui';
import TimeAgo from 'react-timeago'
import './Message.css';
import {socket} from '../actions';
import * as color from 'material-ui/styles/colors'


const styles = {
    container: {
        wordWrap: 'break-word',
        marginBottom: '20px',
        lineHeight: '24px',
        padding: '0 10px',
        clear: 'both'
    },
    timeAgo: {
        color: color.grey700,
        textAlign: 'right',
        fontSize: '11px'
    }
};

export default class Message extends Component {
    static propTypes = {
        message: PropTypes.object.isRequired,
    };

    render() {
        const {message} = this.props;
        return (
            <ListItem
                disabled={true}
                secondaryTextLines={1}
                style={{...styles.container, textAlign: (socket.id === message.user.id) ? 'right' : 'left'}}
            >
                <div className={(socket.id === message.user.id) ? 'from-me' : 'from-them'}>
                    <div>{message.user.username}</div>
                    <div>{message.content}</div>
                    <TimeAgo style={styles.timeAgo} date={message.createdAt}/>
                </div>
            </ListItem>

        );
    }
}
