import {
    RECEIVE_MESSAGES,
    REQUEST_MESSAGES,
    RECEIVE_SINGLE_MESSAGE,
    TYPE_MESSAGE,
    MESSAGE_SENT
} from '../actions/index';

const initialState = {
    isFetching: false,
    all: [],
    input: ''
};


export function messages(state = initialState, action) {
    switch (action.type) {
        case REQUEST_MESSAGES:
            return {
                ...state,
                isFetching: true,
            };
        case RECEIVE_MESSAGES:
            return {
                ...state,
                isFetching: false,
                all: action.messages
            };
        case RECEIVE_SINGLE_MESSAGE: {
            let all = [...state.all];
            all.push(action.message);
            return {
                ...state,
                all
            };
        }
        case TYPE_MESSAGE: {
            return {
                ...state,
                input: action.value
            };
        }
        case MESSAGE_SENT: {
            return {
                ...state,
                input: ''
            };
        }
        default:
            return state;
    }
}