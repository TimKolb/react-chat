import {
    SET_USERNAME,
    LOAD_USERNAME,
    OPEN_MODAL,
    CLOSE_MODAL
} from '../actions/index';

const initialState = {
    registerModal: false
};

export function user(state = initialState, action) {
    switch (action.type) {
        case SET_USERNAME: {
            const username = action.username;
            localStorage.setItem('username', username);
            return {
                ...state,
                username
            };
        }
        case LOAD_USERNAME: {
            let username = localStorage.getItem('username') || 'Unknown User';
            return {
                ...state,
                username
            };
        }
        case OPEN_MODAL: {
            return {
                ...state,
                registerModal: true
            }
        }
        case CLOSE_MODAL: {
            return {
                ...state,
                registerModal: false
            }
        }
        default:
            return state;
    }
}