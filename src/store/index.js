import {createStore, applyMiddleware, compose} from 'redux';
import createLogger from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import reducers from '../reducers';
import {listenForNewMessages, loadUser} from '../actions';
import DevTools from '../components/containers/DevTools';
const loggerMiddleware = createLogger();

const enhancer = compose(
    // Required! Enable Redux DevTools with the monitors you chose
    applyMiddleware(
        thunkMiddleware, // lets us dispatch() functions
        loggerMiddleware // neat middleware that logs actions
    ),
    DevTools.instrument()
);

function configureStore(initialState) {
    // See https://github.com/rackt/redux/releases/tag/v3.1.0
    const store = createStore(reducers, initialState, enhancer);

    // Hot reload reducers (requires Webpack or Browserify HMR to be enabled)
    if (module.hot) {
        module.hot.accept('../reducers', () =>
            store.replaceReducer(require('../reducers')/*.default if you use Babel 6+ */)
        );
    }

    return store;
}

const store = configureStore({});
store.dispatch(listenForNewMessages());
store.dispatch(loadUser());

export default store;